import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const {
      PlayerOneAttack,
      PlayerOneBlock,
      PlayerTwoAttack,
      PlayerTwoBlock,
      PlayerOneCriticalHitCombination,
      PlayerTwoCriticalHitCombination,
    } = controls;

    const playerOne = {
      ...firstFighter,
      block: false,
      critPermision: true,
      healthBar: document.getElementById('left-fighter-indicator'),
    };

    const playerTwo = {
      ...secondFighter,
      block: false,
      critPermision: true,
      healthBar: document.getElementById('right-fighter-indicator'),
    };
    let pressedPlayerTwo = new Set();
    let pressedPlayerOne = new Set();

    window.addEventListener('keydown', (event) => {
      if (pressedPlayerOne.size >3 ) {
        pressedPlayerOne.clear()
      }
      if (pressedPlayerTwo.size >3 ) {
        pressedPlayerTwo.clear()
      }

      pressedPlayerOne.add(event.code);
      pressedPlayerTwo.add(event.code);

      if (pressedPlayerTwo.size === 3) {
        for (let code of PlayerTwoCriticalHitCombination) {
          if (!pressedPlayerTwo.has(code)) {
            pressedPlayerTwo.clear();
            return;
          }
        }

        if (playerTwo.critPermision) {
          playerOne.health = playerOne.health - playerTwo.attack * 2;
          playerTwo.critPermision = false;
          setTimeout(() => {
            playerTwo.critPermision = true;
          }, 10000);
        }

        playerOne.healthBar.style.width = `${(100 / firstFighter.health) * playerOne.health}%`;

        pressedPlayerTwo.clear();
      }

      if (pressedPlayerOne.size === 3) {
        for (let code of PlayerOneCriticalHitCombination) {
          if (!pressedPlayerOne.has(code)) {
            pressedPlayerOne.clear();
            return;
          }
        }

        if (playerOne.critPermision) {
          playerTwo.health = playerTwo.health - playerOne.attack * 2;
          playerOne.critPermision = false;
          setTimeout(() => {
            playerOne.critPermision = true;
          }, 10000);
        }

        playerTwo.healthBar.style.width = `${(100 / secondFighter.health) * playerTwo.health}%`;
        pressedPlayerOne.clear();
      }

      if (event.code === PlayerTwoBlock) {
        playerTwo.block = true;
      }

      if (event.code === PlayerOneBlock) {
        playerOne.block = true;
      }

      if (event.code === PlayerTwoAttack) {
        if (!playerOne.block && !playerTwo.block) {
          playerOne.health = playerOne.health - getDamage(playerTwo, playerOne);
        }
        playerOne.healthBar.style.width = `${(100 / firstFighter.health) * playerOne.health}%`;
      }

      if (event.code === PlayerOneAttack) {
        if (!playerTwo.block && !playerOne.block) {
          playerTwo.health = playerTwo.health - getDamage(playerOne, playerTwo);
        }
        playerTwo.healthBar.style.width = `${(100 / secondFighter.health) * playerTwo.health}%`;
      }

      getWinner(playerOne, playerTwo);
    });

    window.addEventListener('keyup', (event) => {
      if (event.code === PlayerTwoBlock) {
        playerTwo.block = false;
      }

      if (event.code === PlayerOneBlock) {
        playerOne.block = false;
      }
    });

    function getWinner(playerOne, playerTwo) {
      if (playerOne.health <= 0) {
        return resolve(secondFighter);
      }
      if (playerTwo.health <= 0) {
        return resolve(firstFighter);
      }
    }
    // resolve the promise with the winner when fight is over
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
  // return damage
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  return fighter.attack * criticalHitChance;
  // return hit power
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  return fighter.defense * dodgeChance;
  // return block power
}
