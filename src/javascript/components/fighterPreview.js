import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    const fighterImage = createFighterImage(fighter);
    if (position === 'right') {
      fighterImage.style.transform = 'scale(-1, 1)';
    }

    fighterElement.append(fighterImage);
    fighterElement.append(...createFighterProperties(fighter));
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createFighterProperties(fighter) {
  const arrOfProperties = [];


  for (let key in fighter) {
    if (key != '_id' && key != 'source') {
      const propElement = createElement({
        tagName: 'span',
        className: '',
      });

      propElement.innerText = `${key.toUpperCase()} : ${fighter[key]}`;
      propElement.style.backgroundColor = 'white';
      arrOfProperties.push(propElement);
    }
  }
  return arrOfProperties;
}
