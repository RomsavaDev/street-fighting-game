import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  const winElement = createElement({
    tagName: 'span',
    className: 'modal-body',
  });
  winElement.innerText = fighter.name;

  showModal({ title: 'WINNER IS', bodyElement: winElement });

  // call showModal function
  //!done
}
